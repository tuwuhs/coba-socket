

var app = require('http').createServer(handler),
	io = require('socket.io').listen(app),
	fs = require('fs');

var status = 'On';
var port = process.env.PORT || 8080;
app.listen(port);

function handler(req, res) {
	fs.readFile(__dirname + '/index.html', function(err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}

io.configure(function() {
	io.set("transports", ["xhr-polling"]);
	io.set("polling duration", 10);
});

io.sockets.on('connection', function(socket) {
	socket.emit('changeStatus', {status: status});
	socket.on('buttonClick', function(data, clientAck) {
		if (status === 'Off') {
			status = 'On';
		} else {
			status = 'Off';
		}
		
		clientAck({status: status});
		socket.broadcast.emit('changeStatus', {status: status});
	});
});

